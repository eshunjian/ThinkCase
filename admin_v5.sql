﻿# Host: localhost  (Version: 5.7.26)
# Date: 2019-10-31 14:57:28
# Generator: MySQL-Front 5.3  (Build 4.234)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "system_auth"
#

DROP TABLE IF EXISTS `system_auth`;
CREATE TABLE `system_auth` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(20) DEFAULT NULL COMMENT '权限名称',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '权限状态',
  `sort` bigint(20) unsigned DEFAULT '0' COMMENT '排序权重',
  `desc` varchar(255) DEFAULT '' COMMENT '备注说明',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_system_auth_status` (`status`) USING BTREE,
  KEY `idx_system_auth_title` (`title`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='系统-权限';

#
# Data for table "system_auth"
#

INSERT INTO `system_auth` VALUES (1,'Engineer',1,0,'工程师','2019-10-29 21:48:17'),(2,'Manger',1,0,'经理','2019-10-29 21:48:42');

#
# Structure for table "system_auth_node"
#

DROP TABLE IF EXISTS `system_auth_node`;
CREATE TABLE `system_auth_node` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `auth` bigint(20) unsigned DEFAULT NULL COMMENT '角色',
  `node` varchar(200) DEFAULT NULL COMMENT '节点',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_system_auth_auth` (`auth`) USING BTREE,
  KEY `idx_system_auth_node` (`node`(191)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4 COMMENT='系统-权限-授权';

#
# Data for table "system_auth_node"
#

INSERT INTO `system_auth_node` VALUES (1,2,'admin'),(2,2,'admin/auth'),(3,2,'admin/auth/index'),(4,2,'admin/auth/apply'),(5,2,'admin/auth/add'),(6,2,'admin/auth/edit'),(7,2,'admin/auth/refresh'),(8,2,'admin/auth/forbid'),(9,2,'admin/auth/resume'),(10,2,'admin/auth/remove'),(11,2,'admin/config'),(12,2,'admin/config/info'),(13,2,'admin/config/config'),(14,2,'admin/config/file'),(15,2,'admin/index'),(16,2,'admin/index/clearruntime'),(17,2,'admin/index/buildoptimize'),(18,2,'admin/menu'),(19,2,'admin/menu/index'),(20,2,'admin/menu/add'),(21,2,'admin/menu/edit'),(22,2,'admin/menu/resume'),(23,2,'admin/menu/forbid'),(24,2,'admin/menu/remove'),(25,2,'admin/oplog'),(26,2,'admin/oplog/index'),(27,2,'admin/oplog/clear'),(28,2,'admin/oplog/remove'),(29,2,'admin/queue'),(30,2,'admin/queue/index'),(31,2,'admin/queue/redo'),(32,2,'admin/queue/processstart'),(33,2,'admin/queue/processstop'),(34,2,'admin/queue/remove'),(35,2,'admin/user'),(36,2,'admin/user/index'),(37,2,'admin/user/add'),(38,2,'admin/user/edit'),(39,2,'admin/user/pass'),(40,2,'admin/user/forbid'),(41,2,'admin/user/resume'),(42,2,'admin/user/remove'),(43,2,'worksheet'),(44,2,'worksheet/group'),(45,2,'worksheet/group/index'),(46,2,'worksheet/group/info'),(47,2,'worksheet/personal'),(48,2,'worksheet/personal/index'),(49,2,'worksheet/personal/add'),(50,2,'worksheet/personal/edit'),(51,2,'worksheet/personal/state'),(52,2,'worksheet/personal/remove'),(53,2,'worksheet/personal/info'),(58,1,'worksheet'),(59,1,'worksheet/personal'),(60,1,'worksheet/personal/index'),(61,1,'worksheet/personal/add'),(62,1,'worksheet/personal/edit'),(63,1,'worksheet/personal/state'),(64,1,'worksheet/personal/remove'),(65,1,'worksheet/personal/info');

#
# Structure for table "system_config"
#

DROP TABLE IF EXISTS `system_config`;
CREATE TABLE `system_config` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT '' COMMENT '配置名',
  `value` varchar(500) DEFAULT '' COMMENT '配置值',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_system_config_name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8mb4 COMMENT='系统-配置';

#
# Data for table "system_config"
#

INSERT INTO `system_config` VALUES (1,'app_name','ThinkCase'),(2,'site_name','ThinkCase'),(3,'app_version','v1.0.0'),(4,'site_copy','©版权所有 2014-2018 英拿科技'),(5,'site_icon','/upload/decb0fe26fa3f486/b3f6521bf29403c8.png'),(7,'miitbeian','100000'),(8,'storage_type','local'),(9,'storage_local_exts','doc,gif,icon,jpg,mp3,mp4,p12,pem,png,rar'),(10,'storage_qiniu_bucket','https'),(11,'storage_qiniu_domain','用你自己的吧'),(12,'storage_qiniu_access_key','用你自己的吧'),(13,'storage_qiniu_secret_key','用你自己的吧'),(14,'storage_oss_bucket','cuci-mytest'),(15,'storage_oss_endpoint','oss-cn-hangzhou.aliyuncs.com'),(16,'storage_oss_domain','用你自己的吧'),(17,'storage_oss_keyid','用你自己的吧'),(18,'storage_oss_secret','用你自己的吧'),(36,'storage_oss_is_https','http'),(43,'storage_qiniu_region','华东'),(44,'storage_qiniu_is_https','https'),(45,'wechat_mch_id','1332187001'),(46,'wechat_mch_key','A82DC5BD1F3359081049C568D8502BC5'),(47,'wechat_mch_ssl_type','p12'),(48,'wechat_mch_ssl_p12','65b8e4f56718182d/1bc857ee646aa15d.p12'),(49,'wechat_mch_ssl_key','cc2e3e1345123930/c407d033294f283d.pem'),(50,'wechat_mch_ssl_cer','966eaf89299e9c95/7014872cc109b29a.pem'),(51,'wechat_token','mytoken'),(52,'wechat_appid','wx60a43dd8161666d4'),(53,'wechat_appsecret','9978422e0e431643d4b42868d183d60b'),(54,'wechat_encodingaeskey',''),(55,'wechat_push_url','消息推送地址：http://127.0.0.1:8000/wechat/api.push'),(56,'wechat_type','thr'),(57,'wechat_thr_appid','wx60a43dd8161666d4'),(58,'wechat_thr_appkey','5caf4b0727f6e46a7e6ccbe773cc955d'),(60,'wechat_thr_appurl','消息推送地址：http://127.0.0.1:2314/wechat/api.push'),(61,'component_appid','wx28b58798480874f9'),(62,'component_appsecret','8d0e1ec14ea0adc5027dd0ad82c64bc9'),(63,'component_token','P8QHTIxpBEq88IrxatqhgpBm2OAQROkI'),(64,'component_encodingaeskey','L5uFIa0U6KLalPyXckyqoVIJYLhsfrg8k9YzybZIHsx'),(65,'system_message_state','0'),(66,'sms_zt_username','可以找CUCI申请'),(67,'sms_zt_password','可以找CUCI申请'),(68,'sms_reg_template','您的验证码为{code}，请在十分钟内完成操作！'),(69,'sms_secure','可以找CUCI申请'),(70,'store_title','测试商城'),(71,'store_order_wait_time','0.50'),(72,'store_order_clear_time','24.00'),(73,'store_order_confirm_time','60.00'),(74,'sms_zt_username2','可以找CUCI申请2'),(75,'sms_zt_password2','可以找CUCI申请2'),(76,'sms_secure2','可以找CUCI申请2'),(77,'sms_reg_template2','您的验证码为{code}，请在十分钟内完成操作！2'),(78,'michat_appid','2882303761518074614'),(79,'michat_appkey','5861807470614'),(80,'michat_appsecert','CP/WUTUgDuyOxgLQ5ztesg==');

#
# Structure for table "system_data"
#

DROP TABLE IF EXISTS `system_data`;
CREATE TABLE `system_data` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL COMMENT '配置名',
  `value` longtext COMMENT '配置值',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_system_data_name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='系统-数据';

#
# Data for table "system_data"
#

INSERT INTO `system_data` VALUES (1,'menudata','[{\"name\":\"请输入名称\",\"type\":\"scancode_push\",\"key\":\"scancode_push\"}]');

#
# Structure for table "system_log"
#

DROP TABLE IF EXISTS `system_log`;
CREATE TABLE `system_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `node` varchar(200) NOT NULL DEFAULT '' COMMENT '当前操作节点',
  `geoip` varchar(15) NOT NULL DEFAULT '' COMMENT '操作者IP地址',
  `action` varchar(200) NOT NULL DEFAULT '' COMMENT '操作行为名称',
  `content` varchar(1024) NOT NULL DEFAULT '' COMMENT '操作内容描述',
  `username` varchar(50) NOT NULL DEFAULT '' COMMENT '操作人用户名',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COMMENT='系统-日志';

#
# Data for table "system_log"
#

INSERT INTO `system_log` VALUES (1,'admin/login/index','127.0.0.1','系统管理','用户登录系统成功','admin','2019-10-29 21:44:55'),(2,'admin/login/index','127.0.0.1','系统管理','用户登录系统成功','admin','2019-10-30 09:25:40'),(3,'admin/login/index','127.0.0.1','系统管理','用户登录系统成功','admin','2019-10-30 12:29:27'),(4,'admin/login/index','127.0.0.1','系统管理','用户登录系统成功','admin','2019-10-30 22:44:46'),(5,'admin/login/index','127.0.0.1','系统管理','用户登录系统成功','liliangct','2019-10-30 22:53:38'),(6,'admin/login/index','127.0.0.1','系统管理','用户登录系统成功','admin','2019-10-30 22:54:26'),(7,'admin/login/index','127.0.0.1','系统管理','用户登录系统成功','liliangct','2019-10-30 22:58:00'),(8,'admin/login/index','127.0.0.1','系统管理','用户登录系统成功','admin','2019-10-30 22:59:16'),(9,'admin/login/index','127.0.0.1','系统管理','用户登录系统成功','liliangct','2019-10-31 13:40:11'),(10,'admin/login/index','127.0.0.1','系统管理','用户登录系统成功','admin','2019-10-31 13:41:13'),(11,'admin/login/index','127.0.0.1','系统管理','用户登录系统成功','liliangct','2019-10-31 14:13:27'),(12,'admin/login/index','127.0.0.1','系统管理','用户登录系统成功','liliangct','2019-10-31 14:53:55'),(13,'admin/login/index','127.0.0.1','系统管理','用户登录系统成功','admin','2019-10-31 14:54:59');

#
# Structure for table "system_menu"
#

DROP TABLE IF EXISTS `system_menu`;
CREATE TABLE `system_menu` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) unsigned DEFAULT '0' COMMENT '父ID',
  `title` varchar(100) DEFAULT '' COMMENT '名称',
  `node` varchar(200) DEFAULT '' COMMENT '节点代码',
  `icon` varchar(100) DEFAULT '' COMMENT '菜单图标',
  `url` varchar(400) DEFAULT '' COMMENT '链接',
  `params` varchar(500) DEFAULT '' COMMENT '链接参数',
  `target` varchar(20) DEFAULT '_self' COMMENT '打开方式',
  `sort` int(11) unsigned DEFAULT '0' COMMENT '菜单排序',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '状态(0:禁用,1:启用)',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_system_menu_node` (`node`(191)) USING BTREE,
  KEY `idx_system_menu_status` (`status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8mb4 COMMENT='系统-菜单';

#
# Data for table "system_menu"
#

INSERT INTO `system_menu` VALUES (1,0,'后台首页','','','admin/index/main','','_self',500,1,'2018-09-05 17:59:38'),(2,0,'系统管理','','','#','','_self',102,1,'2018-09-05 18:04:52'),(3,4,'系统菜单管理','','layui-icon layui-icon-layouts','admin/menu/index','','_self',1,1,'2018-09-05 18:05:26'),(4,2,'系统配置','','','#','','_self',20,1,'2018-09-05 18:07:17'),(5,12,'系统用户管理','','layui-icon layui-icon-username','admin/user/index','','_self',1,1,'2018-09-06 11:10:42'),(7,12,'访问权限管理','','layui-icon layui-icon-vercode','admin/auth/index','','_self',2,1,'2018-09-06 15:17:14'),(11,4,'系统参数配置','','layui-icon layui-icon-set','admin/config/info','','_self',4,1,'2018-09-06 16:43:47'),(12,2,'权限管理','','','#','','_self',10,1,'2018-09-06 18:01:31'),(27,4,'系统任务管理','','layui-icon layui-icon-log','admin/queue/index','','_self',3,0,'2018-11-29 11:13:34'),(49,4,'系统日志管理','','layui-icon layui-icon-form','admin/oplog/index','','_self',2,1,'2019-02-18 12:56:56'),(62,0,'工单管理','','','#','#','_self',200,1,'2019-10-29 21:50:51'),(63,62,'我的工单','','','#','','_self',0,1,'2019-10-29 21:52:07'),(66,63,'工单列表','','fa fa-table','/worksheet/personal/index','','_self',0,1,'2019-10-29 21:59:44'),(68,62,'团队工单','','','#','','_self',0,1,'2019-10-29 22:06:11'),(69,68,'工单列表','','layui-icon layui-icon-date','worksheet/group/index','','_self',0,1,'2019-10-29 22:06:44');

#
# Structure for table "system_queue"
#

DROP TABLE IF EXISTS `system_queue`;
CREATE TABLE `system_queue` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '任务名称',
  `data` longtext NOT NULL COMMENT '执行参数',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '任务状态(1新任务,2处理中,3成功,4失败)',
  `preload` varchar(500) DEFAULT '' COMMENT '执行内容',
  `time` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '执行时间',
  `double` tinyint(1) DEFAULT '1' COMMENT '单例模式',
  `desc` varchar(500) DEFAULT '' COMMENT '状态描述',
  `start_at` varchar(20) DEFAULT '' COMMENT '开始时间',
  `end_at` varchar(20) DEFAULT '' COMMENT '结束时间',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_system_queue_double` (`double`) USING BTREE,
  KEY `idx_system_queue_time` (`time`) USING BTREE,
  KEY `idx_system_queue_title` (`title`) USING BTREE,
  KEY `idx_system_queue_create_at` (`create_at`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='系统-任务';

#
# Data for table "system_queue"
#


#
# Structure for table "system_user"
#

DROP TABLE IF EXISTS `system_user`;
CREATE TABLE `system_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT '' COMMENT '用户账号',
  `password` varchar(32) DEFAULT '' COMMENT '用户密码',
  `qq` varchar(16) DEFAULT '' COMMENT '联系QQ',
  `mail` varchar(32) DEFAULT '' COMMENT '联系邮箱',
  `phone` varchar(16) DEFAULT '' COMMENT '联系手机',
  `login_at` datetime DEFAULT NULL COMMENT '登录时间',
  `login_ip` varchar(255) DEFAULT '' COMMENT '登录IP',
  `login_num` bigint(20) unsigned DEFAULT '0' COMMENT '登录次数',
  `authorize` varchar(255) DEFAULT '' COMMENT '权限授权',
  `tags` varchar(255) DEFAULT '' COMMENT '用户标签',
  `desc` varchar(255) DEFAULT '' COMMENT '备注说明',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '状态(0禁用,1启用)',
  `is_deleted` tinyint(1) unsigned DEFAULT '0' COMMENT '删除(1删除,0未删)',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `user_dept` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_system_user_username` (`username`) USING BTREE,
  KEY `idx_system_user_status` (`status`) USING BTREE,
  KEY `idx_system_user_deleted` (`is_deleted`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10003 DEFAULT CHARSET=utf8mb4 COMMENT='系统-用户';

#
# Data for table "system_user"
#

INSERT INTO `system_user` VALUES (10000,'admin','21232f297a57a5a743894a0e4a801fc3','22222222','','','2019-10-31 14:54:59','127.0.0.1',669,'','','',1,0,'2015-11-13 15:14:22',2),(10001,'515229989@qq.com','','','515229989@qq.com','18255115257',NULL,'',0,'1','','',1,0,'2019-10-30 22:36:38',1),(10002,'liliangct','e6e061838856bf47e1de730719fb2609','','','','2019-10-31 14:53:55','127.0.0.1',5,'1','','',1,0,'2019-10-30 22:41:58',2);

#
# Structure for table "work_sheet"
#

DROP TABLE IF EXISTS `work_sheet`;
CREATE TABLE `work_sheet` (
  `work_id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `customer` varchar(100) NOT NULL DEFAULT '' COMMENT '服务客户',
  `grade` varchar(20) NOT NULL DEFAULT '' COMMENT '服务等级',
  `categroy` varchar(20) NOT NULL DEFAULT '' COMMENT '服务分类',
  `work_desc` tinytext NOT NULL COMMENT '服务描述',
  `work_content` text COMMENT '工作内容',
  `work_status` tinyint(3) NOT NULL DEFAULT '0' COMMENT '工作结果 0未解决 1延后解决 2已解决',
  `work_start` datetime DEFAULT NULL COMMENT '工作开始时间',
  `work_end` datetime DEFAULT NULL COMMENT '工作结束时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `support` tinyint(1) DEFAULT '0' COMMENT '0 无专家支持  1 专家支持',
  `support_name` varchar(60) DEFAULT NULL COMMENT '支持专家人',
  `customer_phone` char(11) DEFAULT NULL COMMENT '客户联系方式',
  `contact_people` varchar(20) DEFAULT NULL COMMENT '客户联系人',
  `ctime` datetime DEFAULT NULL,
  PRIMARY KEY (`work_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

#
# Data for table "work_sheet"
#

/*!40000 ALTER TABLE `work_sheet` DISABLE KEYS */;
INSERT INTO `work_sheet` VALUES (1,10000,'1','1','1','上证信息维护','<p>1.测试1</p>\r\n\r\n<p>2.测试2</p>\r\n\r\n<p>3.测试3</p>\r\n',2,'2019-10-30 00:00:00','2019-10-31 00:00:00','系统镜像已备份',1,'张三','18000000000','张三',NULL),(2,10002,'2','4','5','丝芙兰Openshift搭建','<p>Openshift集群安装</p>\r\n',0,'2019-10-24 00:00:00','2019-10-28 00:00:00','主机申请',1,'','18000000002','王五',NULL),(3,10002,'3','1','1','权限测试','<p>权限测试</p>\r\n',0,'2019-10-30 00:00:00','2019-10-31 00:00:00','',1,'','18000000000','张三',NULL);
/*!40000 ALTER TABLE `work_sheet` ENABLE KEYS */;
