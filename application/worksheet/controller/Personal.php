<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2019 广州楚才信息科技有限公司 [ http://www.cuci.cc ]
// +----------------------------------------------------------------------
// | 官方网站: http://demo.thinkadmin.top
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/zoujingli/ThinkAdmin
// | github 代码仓库：https://github.com/zoujingli/ThinkAdmin
// +----------------------------------------------------------------------

namespace app\worksheet\controller;

use library\Controller;
use think\Db;

/**
 * 个人工单管理
 * Class Auth
 * @package app\worksheet\controller
 */
class Personal extends Controller
{
    /**
     * 指定数据表
     * @var string
     */
    protected $table = 'WorkSheet';

    /**
     * 个人工单列表
     * @auth true  # 表示需要验证权限
     * @menu true  # 在菜单编辑的节点可选项
     */
    public function index()
    {
        $this->title = '个人工单管理';
        $query = $this->_query($this->table)->where("uid", session('admin_user.id'))->where("work_status", ">=", 0);

        if (!empty($this->request->get("customer"))) {
            $query = $query->where("customer", $this->request->get("customer"));
        }
        if (!empty($this->request->get("phone"))) {
            $query = $query->where("customer_phone", $this->request->get("phone"));
        }
        if (!empty($this->request->get("status"))) {
            $query = $query->where("work_status", $this->request->get("status"));
        }


        $query = $query->order('work_id desc')->page();
    }

    /**
     * 新增个人工单
     * @auth true  # 表示需要验证权限
     * @menu true  # 在菜单编辑的节点可选项
     */
    public function add()
    {
        $this->title = '创建工单';
        $this->applyCsrfToken();
        $this->_form($this->table, 'form');
    }

    /**
     * 编辑个人工单
     * @auth true  # 表示需要验证权限
     * @menu true  # 在菜单编辑的节点可选项
     */
    public function edit()
    {
        $this->title = '修改工单';
        $this->applyCsrfToken();
        $this->_form($this->table, 'form');
    }

    /**
     * 个人工单状态更改
     * @auth true  # 表示需要验证权限
     * @menu true  # 在菜单编辑的节点可选项
     */
    public function state()
    {
        $this->_save($this->table, ['work_status' => input('work_status', '0')]);
    }

    /**
     * 删除个人工单
     * @auth true  # 表示需要验证权限
     * @menu true  # 在菜单编辑的节点可选项
     */
    public function remove()
    {
        $this->_delete($this->table);
    }

    /**
     * 查看个人工单
     * @auth true  # 表示需要验证权限
     * @menu true  # 在菜单编辑的节点可选项
     */
    public function info()
    {
        $this->title = '工单信息表';
        $this->_form($this->table, 'info');
    }

    protected function _form_filter(&$data)
    {
        if (isset($data["support_name"])) {
            $data["support"] = 1;
        }
        if (isset($data["contact_people"])) {
            $data["customer_phone"] = config("contact_people")[$data["contact_people"]];
        }
        $data["uid"] = session("admin_user.id");
    }

}
