<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2019 广州楚才信息科技有限公司 [ http://www.cuci.cc ]
// +----------------------------------------------------------------------
// | 官方网站: http://demo.thinkadmin.top
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/zoujingli/ThinkAdmin
// | github 代码仓库：https://github.com/zoujingli/ThinkAdmin
// +----------------------------------------------------------------------

namespace app\worksheet\controller;

use library\Controller;
use think\Db;

/**
 * 团队工单管理
 * Class Auth
 * @package app\worksheet\controller
 */
class Group extends Controller
{
    /**
     * 指定数据表
     * @var string
     */
    protected $table = 'WorkSheet';

    /**
     * 团队工单列表
     * @auth true  # 表示需要验证权限
     * @menu true  # 在菜单编辑的节点可选项
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function index()
    {
        $this->title = '团队工单管理';
        $user_dept = session("admin_user.user_dept");

        $query = $this->_query($this->table)
            ->alias("a")
            ->join("system_user b", "a.uid=b.id")
            ->where("b.user_dept", $user_dept)
            ->where("a.work_status", ">=", 0);

        if (!empty($this->request->get("customer"))) {
            $query = $query->where("customer", $this->request->get("customer"));
        }
        if (!empty($this->request->get("phone"))) {
            $query = $query->where("customer_phone", $this->request->get("phone"));
        }
        if (!empty($this->request->get("status"))) {
            $query = $query->where("work_status", $this->request->get("status"));
        }

        $query = $query->order('work_id desc')->page();
    }

    /**
     * 团队工单查看
     * @auth true
     * @menu true
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function info()
    {
        $this->title = '工单信息表';
        $this->_form($this->table, 'info');
    }
}
